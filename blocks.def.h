//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "/home/zerotwo/.config/scripts/someblock/audio",			0,		10},
	{"", "/home/zerotwo/.config/scripts/someblock/battery",			30,		0},
    {"", "/home/zerotwo/.config/scripts/someblock/brightness",		0,		13},
	{"", "/home/zerotwo/.config/scripts/someblock/cpu",				30,		0},
	{"", "/home/zerotwo/.config/scripts/someblock/date",			60,		0},
	{"", "/home/zerotwo/.config/scripts/someblock/disk",			300,	0},
	{"", "/home/zerotwo/.config/scripts/someblock/internet",		60,		0},
	{"", "/home/zerotwo/.config/scripts/someblock/memory",		    30,		0},
	{"", "/home/zerotwo/.config/scripts/someblock/temp",			30,		0},
	{"", "/home/zerotwo/.config/scripts/someblock/pkgs",			300,	0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 8;
